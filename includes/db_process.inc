<?php


function skyview_add_database(){

    $database = variable_get('sv_database');
    $username = variable_get('sv_username');
    $password = variable_get('sv_password');
    $host = variable_get('sv_host');
    $driver = variable_get('sv_driver');

    $modules = [];

    $DB_Connection = array(
        'database' => "$database",
        'username' => "$username", // assuming this is necessary
        'password' => "$password", // assuming this is necessary
        'host' => "$host", // assumes localhost
        'driver' => "$driver", // replace with your database driver
    );

    Database::addConnectionInfo("Skyview_$database", 'default', $DB_Connection);
    db_set_active("Skyview_$database");

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']["$database"]["$mod->name"]['name'] = "$mod->name";
        $modules['module']["$database"]["$mod->name"]['site'] = "$database";
        $modules['module']["$database"]["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']["$database"]["$mod->name"]['name'] = "$mod->name";
        $modules['module']["$database"]["$mod->name"]['site'] = "$database";
        $modules['module']["$database"]["$mod->name"]['status'] = 0;
    }

    // RESET TO ORIGINAL DATABASE
    db_set_active();

    foreach ($modules['module'] as $site => $mods) {
        foreach ($modules['module']["$site"] as $key => $value) {

            db_insert('sky_view_modules')
                ->fields(array('site', 'module', 'status', 'valid'))
                ->values(array(
                    'site' => $value['site'],
                    'module' => $value['name'],
                    'status' => $value['status'],
                    'valid' => 'CURRENT_TIMESTAMP',
                ))
                ->execute();
        }
    }

    //Remove the variables so they do not stay in the system.
    variable_del('sv_database');
    variable_del('sv_username');
    variable_del('sv_password');
    variable_del('sv_host');
    variable_del('sv_driver');

    menu_rebuild();
    menu_cache_clear_all();
}














function skyview_snapshot(){
    $database = variable_get('sv_database');
    $username = variable_get('sv_username');
    $password = variable_get('sv_password');
    $host = variable_get('sv_host');
    $driver = variable_get('sv_driver');

    $modules = [];

    $DB_Connection = array(
        'database' => "$database",
        'username' => "$username", // assuming this is necessary
        'password' => "$password", // assuming this is necessary
        'host' => "$host", // assumes localhost
        'driver' => "$driver", // replace with your database driver
    );

    Database::addConnectionInfo("Skyview_$database", 'default', $DB_Connection);
    db_set_active("Skyview_$database");

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']["$database"]["$mod->name"]['name'] = "$mod->name";
        $modules['module']["$database"]["$mod->name"]['site'] = "$database";
        $modules['module']["$database"]["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']["$database"]["$mod->name"]['name'] = "$mod->name";
        $modules['module']["$database"]["$mod->name"]['site'] = "$database";
        $modules['module']["$database"]["$mod->name"]['status'] = 0;
    }

    // RESET TO ORIGINAL DATABASE
    db_set_active();

    foreach ($modules['module'] as $site => $mods) {
        foreach ($modules['module']["$site"] as $key => $value) {

            db_insert('sky_view_modules')
                ->fields(array('site', 'module', 'status', 'valid'))
                ->values(array(
                    'site' => $value['site'],
                    'module' => $value['name'],
                    'status' => $value['status'],
                    'valid' => 'CURRENT_TIMESTAMP',
                ))
                ->execute();
        }
    }

    //Remove the variables so they do not stay in the system.
    variable_del('sv_database');
    variable_del('sv_username');
    variable_del('sv_password');
    variable_del('sv_host');
    variable_del('sv_driver');

    menu_rebuild();
    menu_cache_clear_all();
}





















function sky_view_import_databases(){
    $modules = [];

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cmat']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cmat']["$mod->name"]['site'] = "cmat";
        $modules['module']['cmat']["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cmat']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cmat']["$mod->name"]['site'] = "cmat";
        $modules['module']['cmat']["$mod->name"]['status'] = 0;
    }



    // CISTAR DATABASE
    $cistar = array(
        'database' => 'cistar',
        'username' => 'root', // assuming this is necessary
        'password' => '', // assuming this is necessary
        'host' => 'localhost', // assumes localhost
        'driver' => 'mysql', // replace with your database driver
    );

    Database::addConnectionInfo('SkyviewCistar', 'default', $cistar);
    db_set_active('SkyviewCistar');

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cistar']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cistar']["$mod->name"]['site'] = "cistar";
        $modules['module']['cistar']["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cistar']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cistar']["$mod->name"]['site'] = "cistar";
        $modules['module']['cistar']["$mod->name"]['status'] = 0;
    }


    // CBBG DATABASE
    $cbbg = array(
        'database' => 'cbbg',
        'username' => 'root', // assuming this is necessary
        'password' => '', // assuming this is necessary
        'host' => 'localhost', // assumes localhost
        'driver' => 'mysql', // replace with your database driver
    );

    Database::addConnectionInfo('SkyviewCBBG', 'default', $cbbg);
    db_set_active('SkyviewCBBG');

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cbbg']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cbbg']["$mod->name"]['site'] = "cbbg";
        $modules['module']['cbbg']["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cbbg']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cbbg']["$mod->name"]['site'] = "cbbg";
        $modules['module']['cbbg']["$mod->name"]['status'] = 0;
    }


    // PATHS-UP DATABASE
    $paths = array(
        'database' => 'paths-up',
        'username' => 'root', // assuming this is necessary
        'password' => '', // assuming this is necessary
        'host' => 'localhost', // assumes localhost
        'driver' => 'mysql', // replace with your database driver
    );

    Database::addConnectionInfo('SkyviewPATH', 'default', $paths);
    db_set_active('SkyviewPATH');

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['paths']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['paths']["$mod->name"]['site'] = "paths-up";
        $modules['module']['paths']["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['paths']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['paths']["$mod->name"]['site'] = "paths-up";
        $modules['module']['paths']["$mod->name"]['status'] = 0;
    }



    // CELL-MET DATABASE
    $cell_met = array(
        'database' => 'cell-met',
        'username' => 'root', // assuming this is necessary
        'password' => '', // assuming this is necessary
        'host' => 'localhost', // assumes localhost
        'driver' => 'mysql', // replace with your database driver
    );

    Database::addConnectionInfo('SkyviewCELLMET', 'default', $cell_met);
    db_set_active('SkyviewCELLMET');

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cell-met']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cell-met']["$mod->name"]['site'] = "cell-met";
        $modules['module']['cell-met']["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cell-met']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cell-met']["$mod->name"]['site'] = "cell-met";
        $modules['module']['cell-met']["$mod->name"]['status'] = 0;
    }

    // CBIRC DATABASE
    $cbirc = array(
        'database' => 'cbirc',
        'username' => 'root', // assuming this is necessary
        'password' => '', // assuming this is necessary
        'host' => 'localhost', // assumes localhost
        'driver' => 'mysql', // replace with your database driver
    );

    Database::addConnectionInfo('SkyviewCBIRC', 'default', $cbirc);
    db_set_active('SkyviewCBIRC');

    // SEARCH FOR ACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 1, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cbirc']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cbirc']["$mod->name"]['site'] = "cbirc";
        $modules['module']['cbirc']["$mod->name"]['status'] = 1;
    }

    //SEARCH FOR INACTIVE MODULES
    $q = db_select('system', 's')
        ->fields('s', array('name', 'type', 'status'))
        ->condition('status', 0, '=')
        ->condition('type', 'module', '=')
        ->orderBy('name', 'ASC');
    $r = $q->execute()->fetchAll();

    foreach ($r as $mod){
        $modules['module']['cbirc']["$mod->name"]['name'] = "$mod->name";
        $modules['module']['cbirc']["$mod->name"]['site'] = "cbirc";
        $modules['module']['cbirc']["$mod->name"]['status'] = 0;
    }



    // RESET TO ORIGINAL DATABASE
    db_set_active();

    $truncate = db_truncate('sky_view_modules')->execute();

    foreach ($modules['module'] as $site => $mods) {
        foreach ($modules['module']["$site"] as $key => $value) {

            db_insert('sky_view_modules')
                ->fields(array('site', 'module', 'status', 'valid'))
                ->values(array(
                    'site' => $value['site'],
                    'module' => $value['name'],
                    'status' => $value['status'],
                    'valid' => 'CURRENT_TIMESTAMP',
                ))
                ->execute();
        }
    }
}
